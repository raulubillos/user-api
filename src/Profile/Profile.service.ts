import { Injectable } from "@nestjs/common";
import { ProfileRepo } from "src/repositories/Profile/ProfileRepository.service";
import { ICompleteProfile } from "./interfaces/CompleteProfile.interface";

@Injectable()
export class ProfileService{
    constructor(private profileRepository: ProfileRepo){}

    async findProfile(userId: number):Promise<ICompleteProfile>{
        let profile = await this.profileRepository.findProfile(userId);
        return {
            id: profile.id,
            name: profile.name,
            address: {
                city: profile.address.city.cityName,
                country: profile.address.city.country.countryName,
                street: profile.address.street
            }
        }
    }
}