import { Module } from "@nestjs/common";
import { AuthenticationModule } from "src/helpers/Authentication/Authentication.module";
import { ProfileController } from "./Profile.controller";
import { ProfileRepositoryModule } from "src/repositories/Profile/ProfileRepository.module";
import { ProfileService } from "./Profile.service";

@Module({
 providers:[ProfileService],
 imports:[AuthenticationModule, ProfileRepositoryModule],
 controllers:[ProfileController]
})
export class ProfileModule{}
