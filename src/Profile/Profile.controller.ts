import { Controller, Get, Request, UseGuards } from "@nestjs/common";
import { JwtGuard } from "src/helpers/Authentication/guards/JwtGuard.guard";
import { ICompleteProfile } from "./interfaces/CompleteProfile.interface";
import { ProfileService } from "./Profile.service";


@Controller({path:"/profile"})
export class ProfileController {

    constructor(private profileService:ProfileService){}

    @UseGuards(JwtGuard)
    @Get()
    async profile(@Request() req): Promise<ICompleteProfile>{
        return await this.profileService.findProfile(req.user.userId);
    }

}