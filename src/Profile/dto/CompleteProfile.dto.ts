import { ICompleteProfile } from '../interfaces/CompleteProfile.interface'

export class CompleteProfile implements ICompleteProfile{
    id:number;
    name:string;
    address: {
        street: string;
        city: string;
        country: string;
    }
}