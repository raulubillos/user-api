export class AccessTokenInformation{
    sub: { userId: number };
    username: string;
}