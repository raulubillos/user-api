import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import { Injectable } from '@nestjs/common';
import { jwtConstants } from "../JwtConstants";
import { AccessTokenInformation } from "../dto/AccessTokenInformation.dto";

@Injectable()
export class JwtTokenValidation extends PassportStrategy(Strategy){

    constructor(){
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: jwtConstants.secret
        })
    }

    async validate(payload: AccessTokenInformation){
        return {
            userId: payload.sub.userId
        }
    }

}