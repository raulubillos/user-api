import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-local";
import { UserRepo } from "src/repositories/User/UserRepository.service";

@Injectable()
export class UserValidationStrategy extends PassportStrategy(Strategy){
    constructor(private userRepository: UserRepo){
        super();
    }

    async validate(username:string, password:string): Promise<any>{
        const user = await this.userRepository.findUser({username:username, password:password});
        if(!user){
            throw new UnauthorizedException();
        }
        return user;
    }
}