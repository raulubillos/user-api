import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { AccessToken } from "./dto/AccessToken.dto";
import { AccessTokenInformation } from "./dto/AccessTokenInformation.dto";
@Injectable()
export class AuthenticationService{
    constructor(private jwtService:JwtService){}

    generateAccessToken(info:AccessTokenInformation):AccessToken{
        return {
            access_token: this.jwtService.sign(info)
        }
    }
}