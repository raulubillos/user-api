import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { PassportModule } from "@nestjs/passport";
import { UserRepositoryModule } from "src/repositories/User/UserRepository.module";
import { AuthenticationService } from "./Authentication.service";
import { jwtConstants } from "./JwtConstants";
import { UserValidationStrategy } from "./strategies/UserValidationStrategy.strategy";
import { JwtTokenValidation } from "./strategies/JwtTokenValidation.strategy";

@Module({
    providers:[AuthenticationService,UserValidationStrategy, JwtTokenValidation],
    imports:[
                UserRepositoryModule,
                PassportModule,
                JwtModule.register({
                    secret:jwtConstants.secret,
                    signOptions:{expiresIn: '30m'}
                })
            ],
    exports:[AuthenticationService, UserValidationStrategy, JwtTokenValidation]
})
export class AuthenticationModule{}