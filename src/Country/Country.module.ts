import { Module } from "@nestjs/common";
import { CountryRepositoryModule } from "src/repositories/Country/CountryRepository.module";
import { CountryController } from "./Country.controller";
import { CountryService } from "./Country.service";

@Module({
    controllers:[CountryController],
    providers:[CountryService],
    imports: [CountryRepositoryModule]
})
export class CountryModule{

}