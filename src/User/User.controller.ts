import { Body, Controller, HttpCode, HttpStatus, Post, Request, UseGuards } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { AuthenticationService } from "src/helpers/Authentication/Authentication.service";
import { UserCreation } from "./dto/UserCreation.dto";
import { UserService } from "./User.service";

@Controller({path:"/user"})
export class UserController {

    constructor(private userService: UserService, private authenticationService: AuthenticationService){}

    @Post("/register")
    @HttpCode(HttpStatus.CREATED)
    async register(@Body() userToRegister: UserCreation){
        return await this.userService.create(userToRegister);
    }

    @UseGuards(AuthGuard('local'))
    @Post("/login")
    @HttpCode(HttpStatus.OK)
    login(@Request() req){
        return this.authenticationService.generateAccessToken({
            username: req.user.username,
            sub:{userId:req.user.id}})
    }

}