import { IUserCreation } from "../interfaces/UserCreation.interface";

export class UserCreation implements IUserCreation {
    username: string;
    password: string;
    name: string;
    cityId: number;
    address: string;
}