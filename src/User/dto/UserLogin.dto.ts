
import { IUserLogin } from "../interfaces/UserLogin.interface"; 

export class UserLogin implements IUserLogin{
    username:string;
    password:string;
}