export interface IUserCreation {
    username: string;
    password: string;
    name: string;
    cityId: number;
    address: string;
}