import { Module } from "@nestjs/common";
import { AuthenticationModule } from "src/helpers/Authentication/Authentication.module";
import { UserRepositoryModule } from "src/repositories/User/UserRepository.module";
import { AddressRepositoryModule } from "src/repositories/Address/AddressRepository.module";
import { ProfileRepositoryModule } from "src/repositories/Profile/ProfileRepository.module";
import { UserController } from "./User.controller";
import { UserService } from "./User.service";
import { SequelizeModule } from "@nestjs/sequelize";

@Module({
    controllers: [UserController],
    providers: [UserService],
    imports: [
        UserRepositoryModule,
        AddressRepositoryModule,
        ProfileRepositoryModule, 
        AuthenticationModule
    ]
})
export class UserModule{}