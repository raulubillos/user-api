import { Injectable, InternalServerErrorException } from "@nestjs/common";
import { UserRepo } from "src/repositories/User/UserRepository.service";
import { IUserCreation } from "./interfaces/UserCreation.interface";
import { IUserLogin } from "./interfaces/UserLogin.interface"; 
import { Sequelize } from "sequelize-typescript";
import { AddressRepo } from "src/repositories/Address/AddressRepository.service";
import { ProfileRepo } from "src/repositories/Profile/ProfileRepository.service";

@Injectable()
export class UserService {
    constructor(
        private userRepository: UserRepo, 
        private addressRepository: AddressRepo,
        private profileRepository: ProfileRepo,
        private sequelize:Sequelize
    ){}

    async create(user: IUserCreation) {
        try{
            await this.sequelize.transaction(async transaction => {
                const userId = await this.userRepository.createUser(user, transaction);
                const addressId = await this.addressRepository.createAddress(user,transaction);
                const profile = {
                    userId: userId,
                    addressId: addressId,
                    name: user.name
                };
                await this.profileRepository.createProfile(profile,transaction)
            });
        }catch(error){
            throw InternalServerErrorException;
        }
        
    }

    async findUser(userToFind:IUserLogin): Promise<IUserLogin> {
        return this.userRepository.findUser(userToFind);
    }

}