import { Module } from '@nestjs/common';
import { UserModule } from './User/User.module';
import { ProfileModule } from './Profile/Profile.module';
import { SequelizeModule } from '@nestjs/sequelize';

@Module({
  imports: [
    UserModule, 
    ProfileModule,
    SequelizeModule.forRoot({
      dialect: "mysql",
      host: "localhost",
      database:"users",
      port: 3306,
      username: "root",
      password:"413616Raul",
      autoLoadModels:true,
      synchronize:true
    })
  ],
})
export class AppModule {}
