import { Module } from "@nestjs/common";
import { ProfileRepo } from './ProfileRepository.service';
import { SequelizeModule } from "@nestjs/sequelize";
import { Profile } from "./entities/Profile.model";
import { UserRepositoryModule } from "../User/UserRepository.module";
import { AddressRepositoryModule } from "../Address/AddressRepository.module";

@Module({
    imports: [SequelizeModule.forFeature([Profile]), UserRepositoryModule,AddressRepositoryModule],
    providers:[ProfileRepo],
    exports:[ProfileRepo]
})
export class ProfileRepositoryModule {}