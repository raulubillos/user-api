import { Injectable } from "@nestjs/common";
import { Profile } from "./entities/Profile.model";
import { InjectModel } from '@nestjs/sequelize';
import { Transaction } from "sequelize/types";
import { Address } from "../Address/entities/Address.model";
import { User } from "../User/entities/User.model";
import { City } from "../City/entities/City.model";
import { Country } from "../Country/entities/Country.model";

@Injectable()
export class ProfileRepo{
    constructor(
        @InjectModel(Profile)
        private profileModel: typeof Profile
    ){}

    async createProfile({userId, addressId, name}, transaction: Transaction) {
        const transactionHost = { transaction };
        const profile = await this.profileModel.create({
            userId:userId,
            addressId:addressId,
            name:name
        }, transactionHost);
        return profile.id;
    }

    async findProfile(userId:number) {
        return await this.profileModel.findOne({
            include:[{
                model:Address,
                include:[{
                    model:City,
                    include:[Country]
                }]
            }],
            where:{
                userId
            }
        });
    }
}