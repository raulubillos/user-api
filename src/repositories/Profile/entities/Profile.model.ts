import { Model, Table, HasOne, Column, BelongsTo, ForeignKey } from "sequelize-typescript";
import { Address } from "src/repositories/Address/entities/Address.model";
import { User } from "src/repositories/User/entities/User.model";

@Table
export class Profile extends Model{

    @Column
    name: string;

    @ForeignKey(() => Address)
    @Column
    addressId: number;

    @ForeignKey(() => User)
    @Column
    userId: number;
    
    @BelongsTo(() => Address, { foreignKey:"addressId" })
    address: Address;

    @BelongsTo(() => User, { foreignKey:"userId" })
    user: User;

}