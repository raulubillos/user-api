import { Injectable } from "@nestjs/common";
import { Address } from "./entities/Address.model";
import { InjectModel } from "@nestjs/sequelize";
import { Transaction } from "sequelize/types";

@Injectable()
export class AddressRepo {

    constructor(
        @InjectModel(Address)
        private addressModel: typeof Address
    ) {}

    async createAddress({cityId = 0, address= ""}, transaction:Transaction) {
        const transactionHost = { transaction };
        
        const createdAddress = await this.addressModel.create(
            {
                street: address,
                cityId: cityId
            }, 
            transactionHost
        );

        return createdAddress.id;
    }

}