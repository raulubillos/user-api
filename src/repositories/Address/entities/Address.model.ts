import { Column, Table, Model, ForeignKey, BelongsTo, HasMany } from "sequelize-typescript";
import { City } from "src/repositories/City/entities/City.model";
import { Profile } from "src/repositories/Profile/entities/Profile.model";

@Table({tableName:"Address"})
export class Address extends Model{

    @Column
    street: string;

    @ForeignKey(() => City)
    @Column
    cityId: number;

    @BelongsTo(() => City, "cityId")
    city: City;


}