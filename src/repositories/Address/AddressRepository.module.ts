import { Module } from "@nestjs/common";
import { SequelizeModule } from "@nestjs/sequelize";
import { Address } from "./entities/Address.model";
import { AddressRepo } from "./AddressRepository.service";
import { CityRepositoryModule } from "../City/CityRepository.module";

@Module({
    imports: [SequelizeModule.forFeature([Address]),CityRepositoryModule],
    providers: [AddressRepo],
    exports: [AddressRepo]
})
export class AddressRepositoryModule {}