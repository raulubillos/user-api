import { Module } from "@nestjs/common";
import { SequelizeModule } from "@nestjs/sequelize";
import { UserRepo } from "./UserRepository.service";
import { User } from "./entities/User.model";

@Module({
    imports:[SequelizeModule.forFeature([User])],
    providers:[UserRepo],
    exports:[UserRepo]
})
export class UserRepositoryModule{

}