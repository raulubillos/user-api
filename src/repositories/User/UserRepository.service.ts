import { Injectable } from "@nestjs/common";
import { IUserLogin } from "src/User/interfaces/UserLogin.interface"; 
import { InjectModel } from '@nestjs/sequelize';
import { User } from "./entities/User.model";
import { Transaction } from "sequelize/types";

@Injectable()
export class UserRepo{
    constructor(
        @InjectModel(User)
        private userModel: typeof User
    ){}

    findUser(loginData:IUserLogin): Promise<User>{
        let userFound = this.userModel.findOne({
            where:{
                username: loginData.username,
                password: loginData.password
            }
        })
        if(userFound){
            return userFound;
        }
        return null;
    }

    async createUser({username = "", password = ""}, transaction:Transaction) {
        const transactionHost = { transaction }
        let userInserted = await this.userModel.create (
            {
                username: username,
                password: password
            },
            transactionHost
        )
        return userInserted.id;
    }
}