import { Model, Table, Column } from "sequelize-typescript";

@Table
export class Country extends Model {

    @Column
    countryName:string;
}