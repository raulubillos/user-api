import { Module } from "@nestjs/common";
import { SequelizeModule } from "@nestjs/sequelize";
import { CountryRepo } from "./CountryRepository.service";
import { Country } from "./entities/Country.model";

@Module({
    imports: [SequelizeModule.forFeature([Country])],
    providers: [CountryRepo],
    exports: [CountryRepo]
})
export class CountryRepositoryModule{}