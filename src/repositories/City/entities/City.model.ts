import { Column, Table, Model, HasOne, ForeignKey, BelongsTo } from "sequelize-typescript";
import { Country } from "src/repositories/Country/entities/Country.model";

@Table
export class City extends Model{

    @Column
    cityName: string;

    @ForeignKey(() => Country)
    @Column
    countryId: number;

    @BelongsTo(() => Country, "countryId")
    country: Country;
}