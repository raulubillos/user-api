import { Module } from "@nestjs/common";
import { City } from "./entities/City.model";
import { SequelizeModule } from "@nestjs/sequelize";
import { CountryRepositoryModule } from "../Country/CountryRepository.module";

@Module({
 imports: [SequelizeModule.forFeature([City]),CountryRepositoryModule]
})
export class CityRepositoryModule{}